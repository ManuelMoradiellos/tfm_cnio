# Written by |{- Manuel Moradiellos -}| a.k.a Mr. Verbose

import os
import sys
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

###########
#- Setup -#
###########

# Please change accordingly to your needs, so everything is tidy
input_dir = "Downloads/"
output_dir = "Downloads/loh_counts_output/"
os.makedirs(os.path.dirname(output_dir), exist_ok=True) # Creates output directory

# You can choose to perform the analysis (obtain boolean table/matrix) and dotplot
# according to the type of LOH event of interest:
# 'cnl' : Copy-number-loss ; simple deletion of one allele
# 'cnn' : Copy-number-neutral; deleteion of one allele, duplication of the remaining one 


###############
#-Input Files-#
###############

# Copy number gene-level log2 relative data calculated by GISTIC 2.0,
# obtained from https://gdc.cancer.gov/about-data/publications/pancan-aneuploidy

tcga_df = pd.read_csv(os.path.join(input_dir, 'all_data_by_genes_whitelisted.tsv'),\
    sep = '\t', header = 0, index_col=0) 

## ISSUE: Cannot find much information on what this file is, why does it contain those genes?
##        Needed to understand what we are working with in the first place


# Essential Genes List from genome-wide gene essentiality data from
# loss-of-function and CCLE (Cancer Cell Line Encyclopedia) lines,
# Supp. Data 1 from https://www.nature.com/articles/s41467-020-16399-y 

ess_genes = pd.read_excel(os.path.join(input_dir, '41467_2020_16399_MOESM3_ESM.xlsx'), \
    sheet_name = 0, skiprows = range(5), index_col = 0)  # First five rows are pretty headers, unuseful
ess_genes = ess_genes[ess_genes.columns[-1]]  # Last column is boolean result of essenciality
ess_genes_list = ess_genes[ess_genes == 1].index.tolist() # Len of 1482 instead of 1481 as paper?
                                                          # Apparently 1482 as checked in xlsx


# Results of LOH calling using ABSOLUTE, there's no gene column..
loh_df = pd.read_csv(os.path.join(input_dir,'TCGA_mastercalls.abs_segtabs.fixed.txt'),\
     sep = '\t', header = 0, index_col = 0)

# Survival and clinical information for TCGA samples obtained from XenaBrowser; file obtained
# from https://gitlab.com/ccg-cnio/resources/-/tree/master/TCGA_GAM/resources/MAF_procedure 

patient_info = pd.read_csv(os.path.join(input_dir, 'Survival_SupplementalTable_S1_20171025_xena_sp_1'), \
    sep = '\t', header = 0, index_col = 0)
cancer_type_per_sample = patient_info[['cancer type abbreviation']] # Patient's cancer type
                                                                    # useful for clustering


#######################
#-Files Preprocessing-#
#######################

#tcga_df = tcga_df.set_axis(tcga_df.index.str.replace(r'\|chr[0-9XY]+', ''), axis = 'index')
## ISSUE: Some genes appear two times with different numerical values, some tagged as '|chr0-9X]+'
##        Which are the values that we need? Cannot apply that replace, would broke index
##        example: grep 'FAM138A' all_data_by_genes_whitelisted.tsv | cut -f1-12

tcga_df = tcga_df.drop(['Locus ID', 'Cytoband'], axis = 1)   # Remove unneeded columns
tcga_df_filt = tcga_df <= -0.1   # Copy-loss criteria from authors 
tcga_df_filt = tcga_df_filt.astype(int)   # Turns to boolean table
tcga_df_filt = tcga_df_filt.T    # To have sample/patients as rows

write_table = True # I'm lazy, but stores boolean LOH events table
if write_table:
    tcga_df_filt.to_csv(os.path.join(output_dir, 'all_data_by_genes_whitelisted_loh_booltable.tsv'),
    sep = '\t')  # Takes some time to write

# Before counting LOH events we also keep a filtered df using the paper's 
# essential gene list to check results in Figure 1.f 
tcga_df_filt_essgenes = tcga_df_filt.filter(items = ess_genes_list, axis = 1)

# Now we count all LOH events per patient and add their cancer type for further analysis
def loh_event_counter(dfs):    
    counts_df = pd.DataFrame(index=dfs.index) 
    counts_df['total_LOH_events'] = dfs.sum(axis = 1)   # Count of total events

    counts_df = counts_df.rename(index = lambda x : str(x)[:-13]) # For later indexing
    counts_df = counts_df.join(cancer_type_per_sample)

    return counts_df

loh_count_sample = loh_event_counter(tcga_df_filt)
loh_count_essgenes = loh_event_counter(tcga_df_filt_essgenes)

## LOH is shorter, but here I only work with copy-loss which is one of two mechanisms
## Should I change the name in variables to something more accurate to what I'm portraying??

##########################################
#-Graphical representation of LOH events-#
##########################################


def graphical_repr(df = loh_count_sample, stat = 'mean', saveimage = False,\
    filesufx = 'genes_whitelisted'):
    
    stat = stat.lower()  # Can choose between mean or median to order the results 
    if stat == 'mean':
        df_graph = df.groupby('cancer type abbreviation', sort = False) \
            ['total_LOH_events'].mean().sort_values(ascending = False)
    elif stat == 'median':
        df_graph = df.groupby('cancer type abbreviation', sort = False) \
            ['total_LOH_events'].median().sort_values(ascending = False)
    
    plt.figure(figsize=(15,8))
    ax = sns.stripplot(x = 'cancer type abbreviation', y = 'total_LOH_events', \
        data = df, jitter = 0.1, order = df_graph.index)

    ax.set_title('Copy-number-loss events in {} per sample by TCGA tumor type'.format(filesufx))
    ax.set_xlabel('TCGA tumor type', labelpad = 15)
    ax.set_ylabel('# of Copy-loss events', labelpad = 15)
    plt.xticks(rotation = 90)

    # Adds mean o median bar to each tumor type
    _ = [ax.hlines(y, xmin= i-.25, xmax = i+.25, zorder=3, colors = 'black', label = stat) \
        for i, y in df_graph.reset_index()['total_LOH_events'].items()]

    # For mean o median to appear in a legend
    handles,l = ax.get_legend_handles_labels()
    labels = [stat + 's']
    plt.legend(handles = handles[0:1], labels = labels)

    if saveimage: # To store the results
        plt.subplots_adjust(left = 0.085, right = 0.95, top = 0.915, bottom = 0.14)
        filename = (os.path.join(output_dir, 'loh_events_{}_{}_tsv.png'.format(stat, filesufx)))
        plt.savefig(fname = filename, format = 'png', dpi = 300)

    plt.show()

# Saving plots for all_data_by_genes_whitelisted.tsv LOH events
graphical_repr(stat='mean', saveimage=True)
graphical_repr(stat='median', saveimage=True)

# Saving plots for all_data_by_genes_whitelisted.tsv LOH events on essential genes
# (as in Fig 1.f from the reference paper)
graphical_repr(df = loh_count_essgenes, stat='mean', saveimage=True, \
    filesufx='essential_genes')
graphical_repr(df = loh_count_essgenes,stat='median', saveimage=True,\
    filesufx='essential_genes')
#
## ISSUE: Resulting graph in different order to that of the reference.
#  SOL:   Apparently they don't plot cnl or cnn events at all, they use LOH calling
#         data that they obtain and map to compare to the essential gene list