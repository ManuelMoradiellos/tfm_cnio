from msilib.schema import File
import os
from sqlite3 import paramstyle
import sys
import csv
import math
from xml.etree.ElementTree import tostringlist
import pandas as pd
import matplotlib.pyplot as plt
from pandas.core.frame import DataFrame
from pandas.core.groupby.generic import AggScalar
import seaborn as sns
from collections import defaultdict

os.chdir('../../local/mmoradiellos/work_stuff/1_boolean_table_and_loh_events_count')

#############
#-- Setup --#
#############

# Please change accordingly to your needs, so everything is tidy
input_dir = "input_files/"
output_dir = "./second_task/loh_counts_output/"
os.makedirs(os.path.dirname(output_dir), exist_ok=True) 


###################
#-- Input Files --#
###################

# Essential Genes List from genome-wide gene essentiality data from
# loss-of-function and CCLE (Cancer Cell Line Encyclopedia) lines,
# Supp. Data 1 from https://www.nature.com/articles/s41467-020-16399-y 

ess_genes = pd.read_excel(os.path.join(input_dir, '41467_2020_16399_MOESM3_ESM.xlsx'), \
    sheet_name = 0, skiprows = range(5), index_col = 0)  # First five rows are pretty headers, unuseful
ess_genes = ess_genes[ess_genes.columns[-1]]  # Last column is boolean result of essenciality
ess_genes_list = ess_genes[ess_genes == 1].index.tolist() # Len of 1482 instead of 1481 as paper?
                                                          # Apparently 1482 as checked in xlsx


# Survival and clinical information for TCGA samples obtained from TCGA; file obtained from
# https://gitlab.com/ccg-cnio/resources/-/blob/master/TCGA_GAM/resources/MAF_procedure/combined_study_clinical_data.tsv 
patient_info = pd.read_csv(os.path.join(input_dir, 'Survival_SupplementalTable_S1_20171025_xena_sp_1'), \
    sep = '\t', header = 0, index_col = 0)
cancer_type_per_sample_1 = patient_info[['cancer type abbreviation']] # Patient's cancer type
                                                                      # useful for clustering

patient_info_2 = pd.read_csv(os.path.join(input_dir, 'combined_study_clinical_data.tsv'), \
    sep = '\t', header = 0, index_col = 2)
cancer_type_per_sample_2 = patient_info_2[['TCGA PanCanAtlas Cancer Type Acronym']] 
cancer_type_per_sample_2.rename(columns=\
    {'TCGA PanCanAtlas Cancer Type Acronym':'cancer type abbreviation'}, inplace = True)    

# We merge both tables and remove some duplicates to get the most out of them
cancer_type_per_sample = pd.concat([cancer_type_per_sample_1,cancer_type_per_sample_2],\
    ignore_index = False, sort = True).reset_index().drop_duplicates(subset = 'index').\
        set_index('index')                                                                
cancer_type_per_sample.index.names = ['samples']

# Solip recommended to merge COAD + READ into one class
cancer_type_per_sample['cancer type abbreviation'].\
    replace({'COAD':'COADREAD', 'READ':'COADREAD'}, inplace = True)


# This is a file obtained by Luis which has all the cancer relevant genes
# that the unit usually searches against
res_dir = ('../0_resources/somatic_driver_gene')

input_csv = pd.read_csv(os.path.join(res_dir, 'Cancer_gene_census_3sept_2021_94.tsv'),\
    sep = '\t', header = 0, index_col = None)  

somatic_drivers = input_csv.query('Somatic=="yes"')['Gene Symbol'].tolist() 
germline_drivers = input_csv.query('Germline=="yes"')['Gene Symbol'].tolist()

# List of 152 CPGs used in further parts of the TFM, included to compare the 
# LOH frequency with this gene set to check for possible differences between
# the original LOH matrix (this one) and the merged GAM+LOH matrices for 
# the linear models
cpgs_list = list(pd.read_csv('../0_resources/genes_subsets_ALL_CCG_CPGs/CPG.csv', sep = '\t', header = None, names = ['gene'])['gene'])
cpgs_list = [gene.strip(' ') for gene in cpgs_list] # Remove blank cases just in case

# Although in the GAM matrices we have less number of samples and CPGs as
# some of them weren't found in our final filtered rare pathogenic germline
# variant calling pipeline
cpgs_list_gamsize = pd.read_csv('../4_germline_variant_freq_gam_matrix/out_dir_input_tables_march_25/gam+loh_matrices/loh_binary_table_patient_cpgs_gamdimensions.tsv', sep = '\t', header = 0)
cpgs_list_gamsize_patients = cpgs_list_gamsize['patients'].tolist()
cpgs_list_gamsize_genes = cpgs_list_gamsize.columns[1:len(cpgs_list_gamsize.columns)].tolist()
cpgs_list_gamsize_genes = [gene.strip(' ') for gene in cpgs_list_gamsize_genes] 

# When filtering with both of the lists later on, I found out that we end up missing 24
# genes from our drivers list.
# 
# Checking by hand, some of the genes were mapped with other names, so we'll CHANGE
# THE CURRENT GENECARDS NAMING OF THOSE TO THE ONES FROM OUR LIST
diff_gene_names = {'H3C2':'HIST1H3B', 'H4C9':'HIST1H4I', 'SEPTIN6':'SEPT6',\
    'SEPTIN9':'SEPT9', 'PIGK':'IGK', 'PTCRA' : 'TRA', 'H3-3B':'H3F3B', 'SEPTIN5' : 'SEPT5',
    'H3-3A':'H3F3A'}

# Why some of the missing genes from the lists are not included? 
#
# HMGN2P46 is a pseudogene
# IGL seems to include a family of immunoglobulin-related pseudogenes and prot. coding
#     the protein coding ones are Immunoglobulin Lamba Like [Num] (IGLL-)
#     IGLL1 and IGLL5 are protein coding ones that were mapped and could be included
# IGH seems to be a similar case, some protein coding genes that we currently have
#     mapped are IGHMBP2 and IGHV3OR16-17
# TRD or other gene symbols do not seem to appear in our mapping
# MTCP1 doesn't seem to have mapped in any sample
# MDS2 does not appear as it is a lncRNA
# PTK6 doesn't seem to have mapped in any sample
# TRP are pseudogenes
# ASPSCR1 doesn't seem to have mapped in any sample
# CARS seems to be non-exonic 
# DUX4L1 are a family of pseudogenes
# MALAT1 are lncRNA
# CRLF2 doesn't seem to have mapped in any sample (chr. X)
# P2RY8 doesn't seem to have mapped in any sample (chr. X)



###########################
#-- Files Preprocessing --#
###########################

# Results of LOH calling using ABSOLUTE, there's no gene column and no info on assembly...
##  ISSUE:  If we preprocess it beforehand we can obtain the LOH event number but I
##          don't know how cnl and cnn will be accounted in the matter..
# 'openpyxl' needed

if False: # Just to show how it was done
    loh_df = pd.read_csv(os.path.join(input_dir,'TCGA_mastercalls.abs_segtabs.fixed.txt'),\
        sep = '\t', header = 0, index_col = 0)                                                                

    loh_df.drop(loh_df.columns.difference(['Chromosome', 'Start', 'End', 'LOH']), 1 , inplace = True)
    loh_df.dropna(inplace = True) # Not so many nas
    loh_df = loh_df.astype(int)
    loh_df['Chromosome'].replace({23:'X'}, inplace = True) # Following common criteria, but it
                                                           # wasn't specified by the authors
    # Table reordering and editing
    loh_df['Chromosome'] = 'chr' + loh_df['Chromosome'].astype(str)
    loh_df.reset_index(level=0, inplace=True)
    loh_df = loh_df[['Chromosome', 'Start', 'End', 'LOH', 'Sample']]
    loh_df.rename(columns={'Chromosome': '#Chromosome', 'Start': '#Start', \
        'End':'#End', 'LOH':'#LOH', 'Sample':'#Sample'}, inplace=True)
    loh_df.to_csv(os.path.join(output_dir, 'to_map_nonNA.tsv'), sep = '\t', index=False)



########################
#-- Extra scripting --##
########################

## Some steps were performed outside of python, they appear here to have a cohesive
## step-by-step of how it was done

## To order it as a .BED file
#awk '{first = $1; $1=""; print $0, first}' to_map_nonNA.tsv > to_map_nonNA.bed

#awk -F '[\t]' '$10~ /protein_coding/ {print}' ../gencode.v38.gene.bed > ../gencode.v38.protein_coding.gene.bed 
## Now we just need the coords and gene name
#awk -F '[\t;]' '{print $1,$2,$3,$12}' ../gencode.v38.protein_coding.gene.bed > ../prot_cod_geneCleaned.bed 
## Clean the file up - there are spaces as field separators
#awk -F '[ ]' '{print $1,$2,$3,$6}' ../prot_cod_geneCleaned.bed > ../prot_cod_geneMinimal.bed
## Remove quotes from gene names
#sed 's/"//g' ../prot_cod_geneMinimal.bed > ../prot_cod_final.bed
#sed -i '1 i\#Chromosome\t#Start\t#End\t#Gene' prot_cod_final.bed # Add header
#sed -i 's/ /\t/g' prot_cod_final.bed # Tab delimiter

## We do the mapping of al genes in the chr regions
#bedtools intersect -a to_map_nonNA.bed -b prot_cod_final.bed -wa -wb | awk 'BEGIN {FS=OFS="\t"} {print $5, $9, $4}' > mapped_samples.tsv


## To obtain the final boolean or 0s/1s matrix, R was used thanks to help of Luis.
## I can replicate what I used in python but it is a bit less comprehensive than the
## R code lines...

## One of the encountered issues on creating the matrix was that there were a lot of
## NAs for some patiene-gene entries as the regions they are located within did not
## appeared to be studied in some patients.  In the original study, they used the
## ABSOLUTE algorithm/method for the LOH calls, it seems that they only check some regions
## that may have differing modal absolute copy number of each sample and then double-check
## to see if they're cancerous cell and really have that LOH going on (with various indicators).
## So our criteria is that all genes that appear as NA for a sample weren't suspect of LOH
## to begin with and, thus, we encode them as LOH = 0 (comparing the graphs and freq. tables
## it seems te check out with the references, so it was a good approach )  

## |-{ R CODE }-| ##
#library(dplyr)
#mapping <- read.table("mapped_samples.tsv", header = FALSE, sep ="\t")
#mapping$V1 <- factor(mapping$V1, levels = unique(mapping$V1)) 
#mapping$V3 <- factor(mapping$V3, levels = unique(mapping$V3))
#mapping_1 <- filter(mapping, V2 == 1) ## Only filter LOH == 1

## This is a clever one, it checks wether the Sample-Gene pair appears and writes them
## as LOH = 1 and LOH = 0 if that pair did not appear in the feeded matrix
#table_mapping <- table(mapping_1$V1, mapping_1$V3) 

#dim(table_mapping)  ## [1] 10940 19657   We've got all the samples and genes!
#write.table(table_mapping, file = "loh_matrix_complete.tsv", sep = "\t")

## NEED TO CHANGE 2s to 1s due to duplicate counts from reference algorithm,
## we're just interested in binary event of LOH 

#############################
#-- After extra scripting --#
#############################

matrix_df = pd.read_csv(os.path.join(output_dir,'loh_matrix_complete.tsv'),\
     sep = '\t', header = 0, index_col = 0)                  
matrix_df = matrix_df.astype(int)

# We change the name of the genes mentioned a few section before
matrix_df.rename(columns = diff_gene_names, inplace = True)

# We can now choose the filter to use on the matrix:
def matrix_filter(df = matrix_df, filt = 'essential', write_table = False):
    
    if filt == 'essential':
        # List of 1481 genes from Nichols et al. (2020)
        matrix_filtered_df = df.filter(items = ess_genes_list, axis = 1)

    elif filt == 'somatic':
        # From 'Cancer_gene_census_3sep_2021_94.tsv'
        matrix_filtered_df = df.filter(items = somatic_drivers, axis = 1)

    elif filt == 'germline':
        # From 'Cancer_gene_census_3sep_2021_94.tsv'
        matrix_filtered_df = df.filter(items = germline_drivers, axis = 1)

    elif filt == 'all_drivers':
        # List of 723 genes from 'Cancer_gene_census_3sep_2021_94.tsv'
        drivers_list = list(set(sorted(somatic_drivers + germline_drivers))) 
        matrix_filtered_df = df.filter(items = drivers_list, axis = 1) 

    elif filt == 'essential+drivers':
        all_list = list(set(sorted(somatic_drivers + germline_drivers + ess_genes_list)))
        matrix_filtered_df = df.filter(items = all_list, axis = 1)

    elif filt == 'cpgs':
        # List of 152 CPGs provided by the lab; used to compare with 
        # further steps of the project that compute LOH freq. for this set
        matrix_filtered_df = df.filter(items = cpgs_list, axis = 1)
                     
    if write_table:         
        matrix_filtered_df.to_csv(os.path.join(output_dir, \
            '{}_filtered_loh_booltable.tsv'.format(filt)), sep = '\t') 

    return matrix_filtered_df, filt # Returns 'filt' for file suffixes 


##############################################
#-- Graphical representation of LOH events --#
##############################################

#########   ----------------------------------------------------- 
#-{ 1 }-#  |{ LOH mean|median events in samples per cancer type }|
#########   -----------------------------------------------------

# We filter the matrix to get similar results to those of the reference
matrix_essenfiltered_df, filt = matrix_filter(matrix_df) 
 
# Now we count all loh events per patient and add their cancer type for further analysis
def event_counter_per_sample(dfs):    
    counts_df = pd.DataFrame(index=dfs.index) 
    counts_df['total_loh_events'] = dfs.sum(axis = 1) 
    counts_df = counts_df.join(cancer_type_per_sample)

    return counts_df

loh_count_sample = event_counter_per_sample(matrix_essenfiltered_df)

def graphical_repr_samples(df = loh_count_sample, stat = 'mean', saveimage = False,\
    filesufx = '{}_filtered'.format(filt)):
    
    stat = stat.lower()  # Can choose between mean or median to order the results 
    if stat == 'mean':
        df_graph = df.groupby('cancer type abbreviation', sort = False) \
            ['total_loh_events'].mean().sort_values(ascending = False)
    elif stat == 'median':
        df_graph = df.groupby('cancer type abbreviation', sort = False) \
            ['total_loh_events'].median().sort_values(ascending = False)
    
    plt.figure(figsize=(15,8))
    ax = sns.stripplot(x = 'cancer type abbreviation', y = 'total_loh_events',\
        data = df, jitter = 0.1, order = df_graph.index)

    ax.set_title('LOH events in {} genes per sample by TCGA tumor type'.format(filesufx))
    ax.set_xlabel('TCGA tumor type', labelpad = 15)
    ax.set_ylabel('# of LOH events', labelpad = 15)
    plt.xticks(rotation = 90)

    # Adds mean o median bar to each tumor type
    _ = [ax.hlines(y, xmin= i-.25, xmax = i+.25, zorder=3, colors = 'black', label = stat) \
        for i, y in df_graph.reset_index()['total_loh_events'].items()]

    # For mean o median to appear in a legend
    handles,l = ax.get_legend_handles_labels()
    labels = [stat + 's']
    plt.legend(handles = handles[0:1], labels = labels)

    if saveimage: # To store the results
        plt.subplots_adjust(left = 0.085, right = 0.95, top = 0.915, bottom = 0.14)
        filename = (os.path.join(output_dir, \
            'loh_events_{}_{}_tsv.png'.format(stat, filesufx)))
        plt.savefig(fname = filename, format = 'png', dpi = 300)

    plt.show()

# Saving plots for all_data_by_genes_whitelisted.tsv LOH events
graphical_repr_samples(stat='mean')
graphical_repr_samples(stat='median', saveimage = True) # Really similar to the reference


#########   ------------------------------------------- 
#-{ 2 }-#  |{ LOH frequency in a gene per cancer type }|
#########   -------------------------------------------

# For the example we're going to use the somatic & germline cancer drivers list,
matrix_driversfiltered_df, filt = matrix_filter(matrix_df, filt = 'all_drivers')

# Now we count all loh events per gene and add their cancer type for further analysis

def event_counter_per_gene(dfs, count = 'total', filename = 'drivergenes',\
    write_table = False, round = 3):  
    # Takes aout the information of the samples, and aggregates and sums LOH events
    # in each level for all cancer types
    
    tmp_df = dfs.join(cancer_type_per_sample).reset_index().iloc[:,1:]\
        .set_index('cancer type abbreviation')
    # We lose some of the samples info as we don't have info on their cancer type

    counts_df = tmp_df.groupby('cancer type abbreviation').sum()

    if count == 'freq':        
        # Total of patients with each cancer type
        total_cancer = tmp_df.index.value_counts(sort = False)
        counts_df = round(counts_df.div(total_cancer, axis = 0) * 100, round)
        
    if write_table:
       counts_df.to_csv(os.path.join(output_dir, \
            'loh_{}_events_{}_.tsv'.format(count, filename)), sep = '\t')
    return counts_df

loh_freq_cancertype = event_counter_per_gene(matrix_driversfiltered_df, 'freq')

def pancancer_freq(dfs, decimal = 3):  
    # To give an average freq. in pan-cancer takes out the information of the samples
    # and does not include the cancer type
    
    total_loh_gene = dfs.join(cancer_type_per_sample).reset_index().iloc[:,1:].sum()
    num_samples = dfs.shape[0] - 1
    freq_gene = total_loh_gene.div(num_samples, axis = 0)
    total_freq = freq_gene.sum()

    return round( (total_freq/(dfs.shape[1])) *100, decimal)  

print(f'{pancancer_freq(matrix_driversfiltered_df)}%') # Should return ~15.xx%

# To compare the LOH frequency to the one computed at later steps in the GAM+LOH
# matrices we have to filter out some CPGs and patients that weren't found in any GAM
loh_gam_common = pd.read_csv('../4_germline_variant_freq_gam_matrix/out_dir_input_tables_march_25/gam+loh_matrices/loh_cpgs_common_patient_with_gam.tsv', sep = '\t', header = 0, index_col = 0)
matrix_cpgs_df, filt = matrix_filter(matrix_df, filt = 'cpgs')
matrix_cpgs_df = matrix_cpgs_df.filter(items = cpgs_list_gamsize_genes, axis = 1) 
matrix_cpgs_df.index = [ sample[0:12] for sample in matrix_cpgs_df.index.values] # Quitar dos ultimos numeros del barcode

#matrix_cpgs_df.filter(items= cpgs_list_gamsize_patients, axis = 0) ## Error because of duplicates
sum(matrix_cpgs_df.index.duplicated()) # 315 duplicated indeces
nodup = matrix_cpgs_df[~matrix_cpgs_df.index.duplicated(keep = 'first')] # Drops duplicate keeping first found  10940 --> 10625

matrix_cpgs_df[matrix_cpgs_df.index.isin(cpgs_list_gamsize_patients)] # Returns 9649 and not 9542 as there might be duplicated patient barcodes
nodup[nodup.index.isin(cpgs_list_gamsize_patients)]                   # Nodup returns 9341 when there aren't any duplicates in GAM+LOH. Maybe due to .isin() ?

duplicated_rows = matrix_cpgs_df[matrix_cpgs_df.index.duplicated(keep = 'first')]

some_dups = matrix_cpgs_df[matrix_cpgs_df.index.isin(cpgs_list_gamsize_patients)]
some_dups.to_csv('../4_germline_variant_freq_gam_matrix/input_tables_march_25/loh_cpgs_duppatients.tsv', sep = '\t') # To try and correct the merges