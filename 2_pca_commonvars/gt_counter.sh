#!/bin/bash

INPUT_FILE=$1

COUNT_NA=`grep -o '\./\.' $INPUT_FILE | wc -l`
COUNT_HOM_1=`grep -o '0/0' $INPUT_FILE | wc -l`
COUNT_HET=`grep -o '0/1' $INPUT_FILE | wc -l`
COUNT_HOM_2=`grep -o '1/1' $INPUT_FILE | wc -l`


TOTAL_GT=`echo "$(( $COUNT_NA + $COUNT_HET + $COUNT_HOM_1 + $COUNT_HOM_2 ))"`

awk -vgt=$COUNT_NA -vt=$TOTAL_GT 'BEGIN {printf ("./. : %.0f (%.2f %%) \n", gt, gt*100.0/t)}'
awk -vgt=$COUNT_HOM_1 -vt=$TOTAL_GT 'BEGIN {printf ("0/0 : %.0f (%.2f %%) \n", gt, gt*100.0/t)}'
awk -vgt=$COUNT_HET -vt=$TOTAL_GT 'BEGIN {printf ("0/1 : %.0f (%.2f %%) \n", gt, gt*100.0/t)}'
awk -vgt=$COUNT_HOM_2 -vt=$TOTAL_GT 'BEGIN {printf ("1/1 : %.0f (%.2f %%) \n", gt, gt*100.0/t)}'

